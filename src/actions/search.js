import { fetchSinToken } from '../helpers/fetch';
import { TYPES } from "../types/types";


export const searchAction = ( search ) => {
    return async( dispatch ) => {
        dispatch({
            type: TYPES.GET_ITEMS_SEARCH_START
        })
        const resp = await fetchSinToken( `items?q=${search}`, { }, 'GET' );
        const body = await resp.json();
        if(body.ok){
            dispatch({
                type:TYPES.GET_ITEMS_SEARCH_SUCCESS,
                payload: body.data
            })
        }else {
            dispatch({
                type:TYPES.GET_ITEMS_SEARCH_FAILED,
            })
        }
    }
}
export const resetPathAction = () => dispatch => {
    dispatch({
        type: TYPES.RESET_PATH
    })
}
export const getDetailProductAction = (idProduct) => {
    return async( dispatch ) => {
        dispatch({
            type: TYPES.GET_DETAIL_PRODUCT_START
        })
        const resp = await fetchSinToken( `items/${idProduct}`, { }, 'GET' );
        const body = await resp.json();
        if(body.ok){
            dispatch({
                type:TYPES.GET_DETAIL_PRODUCT_SUCCESS,
                payload: body.data
            })
        }else {
            dispatch({
                type:TYPES.GET_DETAIL_PRODUCT_FAILED,
            })
        }
    }
}
