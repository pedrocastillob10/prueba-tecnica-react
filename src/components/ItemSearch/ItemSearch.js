import React from 'react';
import icon from '../../assets/ic_shipping.png'
import './ItemSearch.scss';
import {formatValueToCOPCurrency} from "../../utils/utils";

export const ItemSearch = ({product, onClick}) => {
    const {picture, free_shipping, title, id, price, city_name} = product;

    const handleClick = () => {
        onClick(id);
    }
    return (
        <div className="row ItemSearch__container" key={id} onClick={handleClick}>
            <div className="col-auto ItemSearch__pr">
                <img
                    className="ItemSearch__img"
                    src={picture}
                    alt="image-product"
                />
            </div>
            <div className="col-9 ItemSearch__description">
                <div className="row">
                    <div className="col-10 mt-3">
                        <span className="ItemSearch__currency">{formatValueToCOPCurrency(price.amount, price.currency )}</span>
                        {free_shipping && (
                            <img
                                className="img-fluid"
                                src={icon}
                                alt="image-free_shipping"
                            />
                        )}
                    </div>
                    <div className="col-2 mt-3"><span className="ItemSearch__location">{city_name}</span></div>
                </div>
                <div className="col-9 ItemSearch__paragraph"> {title}</div>
            </div>
        </div>
    )
}
