import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useParams} from 'react-router-dom';
import {getDetailProductAction} from '../../actions/search';
import {formatValueToCOPCurrency} from '../../utils/utils';

import './DetailProduct.scss'


export const DetailProduct = () => {
    const dispatch = useDispatch();
    const {id: idProduct} = useParams();
    const {itemSelect} = useSelector(state => state.search);

    const {title, description, sold_quantity, condition, price, picture, id} = itemSelect;

    useEffect(() => {
        if (idProduct && price && !price.currency) {
            dispatch(getDetailProductAction(idProduct))
        }
    }, [])

    const renderContent = () => (
        <div className="col-10 offset-1 DetailProduct__container">
            <div className="row">
                <div className="col-8">
                    <img
                        className="DetailProduct__image"
                        src={picture}
                        alt="detailProduct__image"
                    />
                </div>
                <div className="col-4">
                    <div className="row">
                                <span
                                    className="DetailProduct__span-new">{condition === 'new' ? 'Nuevo' : 'Usado'} - {sold_quantity} vendidos</span>
                    </div>
                    <div className="row DetailProduct__row">
                        <h6 className="DetailProduct__title"> {title} </h6>
                    </div>
                    <div className="row DetailProduct__row">
                                <span
                                    className="DetailProduct__span-currency">{formatValueToCOPCurrency(price.amount, price.currency)}</span>
                    </div>
                    <div className="row DetailProduct__row DetailProduct__row__btn">
                        <button type="button" className="btn btn-lg DetailProduct__btn btn-primary">
                            Comprar
                        </button>
                    </div>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-7 DetailProduct__row-description">
                    <h6 className="DetailProduct__h6">Descripción del producto</h6>
                    <p className="DetailProduct__p">{description}</p>
                </div>
            </div>
        </div>
    )
    return (
        <>
            <section className="DetailProduct">
                {id && renderContent()}
            </section>
        </>

    );
}
