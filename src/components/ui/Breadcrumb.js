/* eslint-disable jsx-a11y/anchor-is-valid */
import classNames from 'classnames';
import {v4 as randomUUID} from 'uuid';
import {
    NavLink
} from 'react-router-dom';

export const Breadcrumb = ({routes, className}) => {

    return (
        <div className="col-10 offset-1" >
            <nav aria-label="breadcrumb"
                 className={classNames('Breadcrumb__nav', className)} >
                <ol className="breadcrumb">
                    {routes.map((route) => {
                        let componentName = (
                            <li className="breadcrumb-item" key={`li-${randomUUID().toString()}`}
                                id={randomUUID().toString()}
                            >{route.name}</li>
                        );
                        if (route.path) {
                            componentName = (
                                <NavLink to={route.path}
                                         key={`navlink-${randomUUID().toString()}`}
                                >{route.name}</NavLink>

                            );
                        }
                        if (route.onClick) {
                            componentName = (
                                <a
                                    className="breadcrumb-item"
                                    onClick={route.onClick}
                                    key={`a-${randomUUID().toString()}`}
                                >
                                    {route.name}
                                </a>
                            );
                        }
                        return (
                            <div key={`breadcrumb_component-${randomUUID().toString()}`}>
                                {componentName}
                            </div>
                        );
                    })}
                </ol>
            </nav>
        </div>
    )
}
