import React, {useEffect} from 'react';
import {useHistory} from 'react-router';
import {useDispatch} from 'react-redux';
import { useLocation } from 'react-router-dom';
import {searchAction, resetPathAction} from '../../actions/search';
import {useForm} from '../../hooks/useForm';
import logo from '../../assets/logo.png';
import '../../styles/styles.scss'

export const Navbar = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search)
    const textSearch = queryParams.get("search")

    const [formSearchValues, handleInputChange, reset] = useForm({
        lSearch: textSearch ? textSearch: ''
    });
    const {lSearch} = formSearchValues;
    const handleSubmit = (e) => {
        e.preventDefault();
        if(lSearch.length > 0){
            dispatch(searchAction(lSearch));
            history.push({pathname: '/items', search: `search=${lSearch}`});
        }
    }
    const handleHome = () => {
        history.push('/');
        dispatch(resetPathAction());
        reset({
            lSearch: ''
        })
    }
    useEffect(() => {
        if(textSearch && textSearch.length> 1){
            dispatch(searchAction(textSearch));
            history.push({pathname: '/items', search: `search=${textSearch}`});
        }else{
            handleHome();
        }
    }, [])
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light Nav__container">
            <div className="col-10 offset-1">
                <div className="container-fluid">
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <form className="d-flex Nav__search" onSubmit={handleSubmit} noValidate>
                            <div onClick={handleHome}>
                                <img
                                    className="Nav__logo"
                                    id="header_content__logo_image"
                                    src={logo}
                                    onClick={handleHome}
                                    alt="header_image"
                                />
                            </div>
                            <div className="input-group">
                                <input type="text"
                                       className="form-control Nav__input"
                                       placeholder="Nunca dejes de buscar"
                                       name="lSearch"
                                       onChange={handleInputChange}
                                       value={lSearch}
                                />
                                <div className="input-group-text Nav__button" id="btnGroupAddon" onClick={handleSubmit}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                         fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                        <path
                                            d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                    </svg>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </nav>
    )
}


