import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router';
import {ItemSearch} from '../../components/ItemSearch/ItemSearch'
import {getDetailProductAction} from '../../actions/search';

import './SearchResult.scss'

export const SearchResult = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const {itemsSearch, isLoading} = useSelector(state => state.search);

    const handleDetailProduct = (idProduct) => {
        dispatch(getDetailProductAction(idProduct))
        history.push(`/items/${idProduct}`);
    }

    const renderItenSearch = () => (
        itemsSearch.map(item => (
            <ItemSearch key={item.id}
                        product={item}
                        onClick={idProduct => handleDetailProduct(idProduct)}/>
        ))
    );

    const renderLoader = () => (
    <div className="d-flex justify-content-center col-12">
        <div className="spinner-border text-warning SearchResult__loader" role="status">
            <span className="visually-hidden">Loading...</span>
        </div>
    </div>
    )
    return (
        <section className="SearchResult">
            <div className="col-10 offset-1 SearchResult__container">
                {
                   isLoading ? renderLoader() : renderItenSearch()
                }
            </div>
        </section>
    )
};


