import {TYPES} from "../../types/types";


const initialState = {
    search: null,
    itemsSearch: [],
    categories: [],
    itemSelect: {
        title: null,
        description: null,
        sold_quantity: null,
        condition: null,
        price: {
            amount: null,
            currency: null
        },
        picture: null
    },
    isLoading: false,
    isError: false,
    path: []
}

const searchReducer = (state, { type, payload }) => {
    const localState = state || initialState;
    switch (type) {
        case TYPES.GET_ITEMS_SEARCH_START:
            return {
                ...localState,
                isLoading: true
            };
        case TYPES.GET_ITEMS_SEARCH_SUCCESS:
            return {
                ...localState,
                isLoading: false,
                itemsSearch: payload.items,
                categories: payload.categories,
                path: payload.path
            }
        case TYPES.GET_ITEMS_SEARCH_FAILED:
            return {
                ...localState,
                isLoading: false,
                isError: true
            }
        case TYPES.GET_DETAIL_PRODUCT_START:
            return {
                ...localState,
                isLoading: true
            }
        case TYPES.GET_DETAIL_PRODUCT_SUCCESS:
            return {
                ...localState,
                isLoading: false,
                itemSelect: payload.item
            }
        case TYPES.GET_DETAIL_PRODUCT_FAILED:
            return {
                ...localState,
                isLoading: false,
                isError: true,
                itemSelect: null
            };
        case TYPES.RESET_PATH:
            return {
                ...localState,
                path: []
            }
        default:
            return localState;
    }
}

export default searchReducer;
