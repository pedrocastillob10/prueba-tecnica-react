import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import {useSelector} from 'react-redux';
import {Navbar} from '../components/ui/Navbar';
import {SearchResult} from '../pages/SearchResult/ SearchResult';
import {DetailProduct} from '../components/detailProduct/DetailProduct';
import {Breadcrumb} from '../components/ui/Breadcrumb';


export const Navigation = () => {
    const {path} = useSelector(state => state.search);
  return (
        <Router>
            <div className="main-layout">
                <Navbar />
                {path.length > 0 && (<Breadcrumb routes={path} />)}
                <Switch>
                    <Route path="/items/:id">
                        <DetailProduct/>
                    </Route>
                    <Route path="/items">
                        <SearchResult />
                    </Route>
                    <Route path="/">
                        <div className="Nav__container-search">
                        </div>
                    </Route>
                </Switch>
            </div>
        </Router>
);
}
