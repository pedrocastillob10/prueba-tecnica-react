export const formatValueToCOPCurrency = (value, currency) => {
    return new Intl.NumberFormat('es-AR', {
        style: 'currency',
        currency: currency
    }).format(value);
};
